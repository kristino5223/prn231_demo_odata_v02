﻿using Demo_OData_02.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.OData.Query;
using Microsoft.AspNetCore.OData.Routing.Controllers;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Demo_OData_02.Controllers
{
    public class PressesController : Controller
    {
        private BookStoreContext _db;
        public PressesController(BookStoreContext context)
        {
            _db = context;
           
            if (context.Books.Count() == 0)
            {
                foreach (var item in DataSource.GetBooks())
                {
                    context.Books.Add(item);
                    context.Presses.Add(item.Press);
                }
                context.SaveChanges();
            }
        }

        [EnableQuery]
        public IActionResult Get()
        {
            return Ok(_db.Presses);
        }
    }
}
