﻿namespace Demo_OData_02.Models
{
    public class Address
    {
        public string City { get; set; }
        public string Street { get; set; }
    }
}
