﻿using System.Collections;
using System.Collections.Generic;

namespace Demo_OData_02.Models
{
    public static class DataSource
    {
        private static IList<Book> _book { get; set; }
        public static IList<Book> GetBooks()
        {
            if (_book != null)
            {
                return _book;
            }
            _book = new List<Book>();
            //book #1
            Book book = new Book
            {
                Id = 1,
                ISBN = "978-0-321-87758-1",
                Title= "C# hoc app luc qua",
                Author = "ThanhTH",
                Price = 59.99m,
                Location = new Address
                {
                    City = "HCM City",
                    Street = "Duong vao tim em"
                },
                Press = new Press
                {
                    Id = 1,
                    Name = "TienNL",
                    Category = Category.Book
                }
            };
            _book.Add(book);
            return _book;
        }
    }
}
